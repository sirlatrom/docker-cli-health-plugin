package main

import (
	"bytes"
	"fmt"
	"strconv"
	"strings"
	"time"

	"golang.org/x/crypto/ssh/terminal"

	"github.com/docker/docker/api/types/filters"

	"github.com/docker/cli/cli/command"
	"github.com/docker/cli/cli/command/formatter"
	"github.com/docker/cli/cli/command/inspect"
	units "github.com/docker/go-units"
)

const (
	defaultHealthLogsTableFormat                  = "table {{.ContainerName}}\t{{.Node}}\t{{.CreatedAt}}\t{{.Timestamp}}\t{{.TaskState}}\t{{.ExitCode}}\t{{.LogOutput}}"
	taskIDHeader                                  = "ID"
	taskCreatedHeader                             = "CREATED"
	taskUpdatedHeader                             = "UPDATED"
	healthInspectPrettyTemplate  formatter.Format = `Task ID:        {{.TaskID}}
Container Name:  {{.ContainerName}}
{{- if .ContainerLabels }}
Container labels:
{{- range $k, $v := .ContainerLabels }}
 - {{ $k }}{{if $v }}={{ $v }}{{ end }}
{{- end }}{{ end }}
Task state:      {{.TaskState}}
{{- with $err := .TaskErr}}
Task error:      {{$err}}
{{- end }}
Node:            {{.Node}}
Task created at: {{.CreatedAt}}
Health command:  {{.HealthCommand}}
Container health logs:
{{.HealthLogs}}`
	healthLogsRawTemplate = `{{.Result.Output}}`
)

// NewFormat returns a Format for rendering using a config Context
func NewFormat(source string, raw bool) formatter.Format {
	switch source {
	case formatter.PrettyFormatKey:
		return healthInspectPrettyTemplate
	case formatter.TableFormatKey:
		if raw {
			return healthLogsRawTemplate
		}
		return defaultHealthLogsTableFormat
	}
	return formatter.Format(source)
}

// FormatWrite writes the context
func FormatWrite(ctx formatter.Context, results TaskHealthResultSlice, filters filters.Args) error {
	render := func(format func(subContext formatter.SubContext) error) error {
		for _, result := range results {
			if result.Result != nil {

				if found, err := validExitCode(filters, result.Result.ExitCode); err != nil {
					return err
				} else if !found {
					continue
				}
			}
			if !validNode(filters, result.NodeID, result.NodeHostname) {
				continue
			}
			if !validDesiredState(filters, string(result.TaskDesiredState)) {
				continue
			}
			if !validState(filters, string(result.TaskState)) {
				continue
			}

			healthLogsCtx := &healthLogsContext{TaskHealthResult: result, Trunc: ctx.Trunc}
			if err := format(healthLogsCtx); err != nil {
				return err
			}
		}
		return nil
	}
	return ctx.Write(newHealthLogsContext(), render)
}

func newHealthLogsContext() *healthLogsContext {
	cCtx := &healthLogsContext{}

	cCtx.Header = formatter.SubHeaderContext{
		"ContainerName": "CONTAINER NAME/TASK ID",
		"Node":          "NODE",
		"CreatedAt":     taskCreatedHeader,
		"Timestamp":     "TIMESTAMP",
		"TaskState":     "TASK STATE",
		"ExitCode":      "EXIT CODE",
		"LogOutput":     "HEALTH LOG OUTPUT",
	}
	return cCtx
}

type healthLogsContext struct {
	formatter.HeaderContext
	TaskHealthResult
	Trunc bool
}

func (c *healthLogsContext) MarshalJSON() ([]byte, error) {
	return formatter.MarshalJSON(c.TaskHealthResult)
}

func (c *healthLogsContext) TaskID() string {
	return c.TaskHealthResult.TaskID
}

func (c *healthLogsContext) ContainerName() string {
	if len(c.TaskHealthResult.ContainerName) == 0 {
		return c.TaskHealthResult.TaskID
	}
	return c.TaskHealthResult.ContainerName
}

func (c *healthLogsContext) Node() string {
	if c.TaskHealthResult.NodeHostname != "" {
		return c.TaskHealthResult.NodeHostname
	}
	return c.TaskHealthResult.NodeID
}

func (c *healthLogsContext) CreatedAt() string {
	return units.HumanDuration(time.Now().UTC().Sub(c.TaskHealthResult.TaskCreatedAt)) + " ago"
}

func (c *healthLogsContext) Timestamp() string {
	if c.TaskHealthResult.Result == nil {
		return units.HumanDuration(time.Now().UTC().Sub(c.TaskHealthResult.TaskTimestamp)) + " ago"
	}
	return units.HumanDuration(time.Now().UTC().Sub(c.Result.Start)) + " ago"
}

func (c *healthLogsContext) Labels() string {
	mapLabels := c.TaskHealthResult.ContainerLabels
	if mapLabels == nil {
		return ""
	}
	var joinLabels []string
	for k, v := range mapLabels {
		joinLabels = append(joinLabels, fmt.Sprintf("%s=%s", k, v))
	}
	return strings.Join(joinLabels, ",")
}

func (c *healthLogsContext) Label(name string) string {
	if c.TaskHealthResult.ContainerLabels == nil {
		return ""
	}
	return c.TaskHealthResult.ContainerLabels[name]
}

func (c *healthLogsContext) ExitCode() string {
	if c.TaskHealthResult.Result == nil {
		if c.TaskHealthResult.TaskErr != "" {
			return "Task error"
		}
		return ""
	}
	return strconv.Itoa(c.TaskHealthResult.Result.ExitCode)
}

func (c *healthLogsContext) LogOutput() string {
	if c.TaskHealthResult.Result == nil {
		return strings.TrimSpace(c.TaskHealthResult.TaskErr)
	}
	logOutput := strings.TrimSpace(c.Result.Output)
	if c.Trunc {
		logOutput = formatter.Ellipsis(logOutput, 30)
	}
	return logOutput
}

// InspectFormatWrite renders the context for a list of configs
func InspectFormatWrite(ctx formatter.Context, ref string, getRef inspect.GetRefFunc, filters filters.Args) error {
	if ctx.Format != healthInspectPrettyTemplate {
		return inspect.Inspect(ctx.Output, []string{ref}, string(ctx.Format), getRef)
	}
	render := func(format func(subContext formatter.SubContext) error) error {
		taskHealthSummaryI, _, err := getRef(ref)
		if err != nil {
			return err
		}
		taskHealthSummary, ok := taskHealthSummaryI.(TaskHealthSummary)
		if !ok {
			return fmt.Errorf("got wrong object to inspect :%v", ok)
		}
		if !validNode(filters, taskHealthSummary.NodeID, taskHealthSummary.NodeHostname) {
			return fmt.Errorf("Error: No task or service found for filtered nodes")
		}
		if !validDesiredState(filters, string(taskHealthSummary.TaskDesiredState)) {
			return fmt.Errorf("Error: No task or service found for filtered desired states")
		}
		if !validState(filters, string(taskHealthSummary.TaskState)) {
			return fmt.Errorf("Error: No task or service found for filtered states")
		}
		if err := format(&healthInspectContext{
			TaskHealthSummary: taskHealthSummary,
			Filters:           filters,
			Trunc:             ctx.Trunc,
		}); err != nil {
			return err
		}
		return nil
	}
	return ctx.Write(&healthInspectContext{}, render)
}

func validExitCode(filters filters.Args, exitCode int) (bool, error) {
	validExitCodes := filters.Get("exit-code")
	invalidExitCodes := filters.Get("exit-code!")
	exitCodeFound := true
	if len(validExitCodes) > 0 {
		exitCodeFound = false
		for _, filterValue := range validExitCodes {
			value, err := strconv.Atoi(filterValue)
			if err != nil {
				return false, fmt.Errorf("Invalid exit code %q (must be an integer between 0 and 255): %v", filterValue, err)
			}
			if value < 0 || value > 255 {
				return false, fmt.Errorf("Invalid exit code %v (must be an integer between 0 and 255)", value)
			}
			if exitCode == value {
				exitCodeFound = true
				break
			}
		}
	}
	if exitCodeFound {
		for _, filterValue := range invalidExitCodes {
			value, err := strconv.Atoi(filterValue)
			if err != nil {
				return false, fmt.Errorf("Invalid exit code (must be an integer between 0 and 255): %v", err)
			}
			if value < 0 || value > 255 {
				return false, fmt.Errorf("Invalid exit code (must be an integer between 0 and 255)")
			}
			if exitCode == value {
				exitCodeFound = false
				break
			}
		}
	}
	return exitCodeFound, nil
}

func validNode(filters filters.Args, nodeID string, nodeHostname string) bool {
	found := true
	if filters.Contains("node") {
		found = filters.FuzzyMatch("node", nodeID) || filters.FuzzyMatch("node", nodeHostname)
	}
	if found {
		return filters.FuzzyMatch("node!", nodeID) || filters.FuzzyMatch("node", nodeHostname)
	}
	return found
}

func validDesiredState(filters filters.Args, desiredState string) bool {
	found := true
	if filters.Contains("desired-state") {
		found = filters.FuzzyMatch("desired-state", desiredState)
	}
	if found {
		return filters.FuzzyMatch("desired-state!", desiredState)
	}
	return found
}

func validState(filters filters.Args, state string) bool {
	found := true
	if filters.Contains("state") {
		found = filters.FuzzyMatch("state", state)
	}
	if found {
		return filters.FuzzyMatch("state!", state)
	}
	return found
}

type healthInspectContext struct {
	TaskHealthSummary
	formatter.SubContext
	Filters filters.Args
	Trunc   bool
}

func (ctx *healthInspectContext) TaskID() string {
	return ctx.TaskHealthSummary.TaskID
}

func (ctx *healthInspectContext) ContainerName() string {
	return ctx.TaskHealthSummary.ContainerName
}

func (ctx *healthInspectContext) ContainerLabels() map[string]string {
	return ctx.TaskHealthSummary.ContainerLabels
}

func (ctx *healthInspectContext) Node() string {
	if ctx.TaskHealthSummary.NodeHostname != "" {
		return ctx.TaskHealthSummary.NodeHostname
	}
	return ctx.TaskHealthSummary.NodeID
}

func (ctx *healthInspectContext) TaskErr() string {
	return ctx.TaskHealthSummary.TaskErr
}

func (ctx *healthInspectContext) CreatedAt() string {
	return command.PrettyPrint(ctx.TaskCreatedAt.Local())
}

func (ctx *healthInspectContext) HealthCommand() string {
	cmd := ctx.TaskHealthSummary.HealthCmd
	if cmd != nil {
		return fmt.Sprintf("`%s` every %v (timeout %v) after %v, %v retries", strings.Join(cmd.Test[1:], " "), cmd.Interval, cmd.Timeout, cmd.StartPeriod, cmd.Retries)
	}
	return "<Container's .Config.Healthcheck is empty>"
}

func (ctx *healthInspectContext) HealthLogs() string {
	if len(ctx.Entries) == 0 {
		return "<No health state available>"
	}
	var b []byte
	output := bytes.NewBuffer(b)
	for i := 0; i < len(ctx.Entries); i++ {
		if found, err := validExitCode(ctx.Filters, ctx.Entries[i].ExitCode); err != nil {
			return err.Error()
		} else if !found {
			continue
		}
		fmt.Fprintf(output, " - %v (%v) %s\n", ctx.Entries[i].Start, ctx.Entries[i].End.Sub(ctx.Entries[i].Start), fmt.Sprintf("Exit code %d", ctx.Entries[i].ExitCode))

		logOutput := strings.TrimSpace(ctx.Entries[i].Output)
		if ctx.Trunc {
			width, _, err := terminal.GetSize(0)
			if err != nil {
				width = 80
			}
			logOutput = formatter.Ellipsis(logOutput, width-3)
		}
		fmt.Fprintf(output, "   %s\n", logOutput)
	}
	return output.String()
}
