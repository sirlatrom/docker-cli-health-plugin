package main

import (
	"fmt"
	"strings"

	"github.com/docker/cli/cli"
	"github.com/docker/cli/cli/command"
	"github.com/docker/cli/cli/command/formatter"
	"github.com/docker/cli/opts"
	"github.com/spf13/cobra"
)

// HealthInspectOptions contains the options for the `health inspect` command
type HealthInspectOptions struct {
	TaskID    string
	Format    string
	Pretty    bool
	Filter    opts.FilterOpt
	NoResolve bool
	NoTrunc   bool
}

func healthInspectCmd(dockerCli command.Cli) *cobra.Command {
	options := HealthInspectOptions{Filter: opts.NewFilterOpt()}

	cmd := &cobra.Command{
		Use:     "inspect [OPTIONS] SERVICE|TASK",
		Aliases: []string{"i"},
		Short:   "Inspect health of a task",
		Args:    cli.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			options.TaskID = args[0]
			return RunHealthInspect(dockerCli, options)
		},
	}
	flags := cmd.Flags()
	flags.StringVar(&options.Format, "format", "", "Format the output using the given Go template")
	flags.BoolVar(&options.Pretty, "pretty", false, "Print the information in a human friendly format")
	flags.VarP(&options.Filter, "filter", "f", "Filter output based on conditions provided")
	flags.BoolVar(&options.NoResolve, "no-resolve", false, "Do not map IDs to Names")
	flags.BoolVar(&options.NoTrunc, "no-trunc", false, "Don't truncate output")

	return cmd
}

// RunHealthInspect inspects the health of the given Swarm task.
func RunHealthInspect(dockerCli command.Cli, options HealthInspectOptions) error {
	client := dockerCli.Client()

	if options.Pretty {
		options.Format = "pretty"
	}

	getRef := func(id string) (interface{}, []byte, error) {
		return buildTaskHealthSummary(client, id, options.NoResolve, options.Filter.Value())
	}
	f := options.Format

	// check if the user is trying to apply a template to the pretty format, which
	// is not supported
	if strings.HasPrefix(f, "pretty") && f != "pretty" {
		return fmt.Errorf("Cannot supply extra formatting options to the pretty template")
	}

	configCtx := formatter.Context{
		Output: dockerCli.Out(),
		Format: NewFormat(f, false),
		Trunc:  !options.NoTrunc,
	}

	if err := InspectFormatWrite(configCtx, options.TaskID, getRef, options.Filter.Value()); err != nil {
		return cli.StatusError{StatusCode: 1, Status: err.Error()}
	}
	return nil
}
