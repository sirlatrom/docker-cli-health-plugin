package main

import (
	"context"
	"encoding/json"
	"fmt"
	"sort"

	"github.com/docker/docker/api/types/filters"

	"github.com/docker/cli/cli-plugins/manager"
	"github.com/docker/cli/cli-plugins/plugin"
	"github.com/docker/cli/cli/command"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/swarm"
	"github.com/docker/docker/client"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

var (
	version = "development"
	log     = logrus.New()
	format  string
	verbose bool
)

// DockerCLIPluginMetadata contains the required fields for informing the Docker Engine about the plugin.
type DockerCLIPluginMetadata struct {
	SchemaVersion    string
	Vendor           string
	ShortDescription string
	Version          string
	URL              string
}

func buildTaskHealthSummary(apiClient client.APIClient, taskID string, noResolve bool, filters filters.Args) (TaskHealthSummary, []byte, error) {
	summaries, output, err := buildTaskHealthSummaries(apiClient, []string{taskID}, noResolve, filters)
	if err != nil {
		return TaskHealthSummary{}, nil, err
	}
	if len(summaries) == 0 {
		if filters.Len() > 0 {
			return TaskHealthSummary{}, nil, fmt.Errorf("Error: No task or service found with given filters: %q", taskID)
		}
		return TaskHealthSummary{}, nil, fmt.Errorf("Error: No task or service found: %q", taskID)
	}
	return summaries[0], output, err
}

func buildTaskHealthSummaries(apiClient client.APIClient, taskIDs []string, noResolve bool, commandFilters filters.Args) (TaskHealthSummarySlice, []byte, error) {
	results := TaskHealthSummarySlice{}
	tasks := []swarm.Task{}
	for _, taskID := range taskIDs {
		task, _, err := apiClient.TaskInspectWithRaw(context.Background(), taskID)
		if err != nil {
			if !client.IsErrNotFound(err) {
				return nil, nil, err
			}
			filters := filters.NewArgs(filters.Arg("service", taskID))
			for _, key := range []string{"name", "id", "label", "service", "node", "desired-state", "runtime"} {
				if commandFilters.Contains(key) {
					for _, value := range commandFilters.Get(key) {
						filters.Add(key, value)
					}
				}
			}
			serviceTasks, serviceErr := apiClient.TaskList(context.Background(), types.TaskListOptions{
				Filters: filters,
			})
			if serviceErr != nil {
				return nil, nil, fmt.Errorf("After %q: %v", err, serviceErr)
			}
			tasks = append(tasks, serviceTasks...)
			continue
		}
		tasks = append(tasks, task)
	}
	for _, task := range tasks {
		result := TaskHealthSummary{
			taskHealthMeta: taskHealthMeta{
				TaskID:           task.ID,
				TaskCreatedAt:    task.CreatedAt,
				TaskDesiredState: task.DesiredState,
				TaskState:        task.Status.State,
				TaskTimestamp:    task.Status.Timestamp,
				TaskErr:          task.Status.Err,
				NodeID:           task.NodeID,
			},
		}
		if containerStatus := task.Status.ContainerStatus; containerStatus != nil {
			containerID := containerStatus.ContainerID
			c, _, err := apiClient.ContainerInspectWithRaw(context.Background(), containerID, false)
			if err != nil && !client.IsErrNotFound(err) {
				return nil, nil, err
			}
			if !client.IsErrNotFound(err) {
				result.ContainerID = c.ID
				result.ContainerName = c.Name[1:]
				result.ContainerLabels = c.Config.Labels
				result.HealthCmd = c.Config.Healthcheck
				if c.State.Health != nil {
					result.Status = c.State.Health.Status
					result.FailingStreak = c.State.Health.FailingStreak
					result.Entries = c.State.Health.Log
					sort.Slice(result.Entries, func(i, j int) bool {
						return result.Entries[i].Start.Before(result.Entries[j].Start)
					})
					for i, j := 0, len(result.Entries)-1; i < j; i, j = i+1, j-1 {
						result.Entries[i], result.Entries[j] = result.Entries[j], result.Entries[i]
					}
				}
			}
		}

		if !noResolve {
			node, _, err := apiClient.NodeInspectWithRaw(context.Background(), task.NodeID)
			if err != nil && !client.IsErrNotFound(err) {
				return nil, nil, err
			}
			if !client.IsErrNotFound(err) {
				result.NodeHostname = node.Description.Hostname
			}
			// service, _, err := apiClient.ServiceInspectWithRaw(context.Background(), task.ServiceID, types.ServiceInspectOptions{})
			// if err != nil {
			// 	return nil, nil, err
			// }
			// result.TaskName = fmt.Sprintf("%v.%v", service.Spec.Annotations.Name, task.Slot)
		}
		results = append(results, result)
	}

	sort.Sort(sort.Reverse(results))

	bytesOut, err := json.MarshalIndent(results, "", "    ")
	if err != nil {
		return nil, nil, err
	}

	return results, bytesOut, nil
}

func main() {
	plugin.Run(func(dockerCli command.Cli) *cobra.Command {
		return NewRootCmd("health", true, dockerCli)
	},
		manager.Metadata{
			SchemaVersion: "0.1.0",
			Vendor:        "Alm. Brand",
			Version:       version,
		})
}
