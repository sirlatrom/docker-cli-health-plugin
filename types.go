package main

import (
	"time"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/swarm"
)

// TaskHealthAggregate contains the fields for aggregating the health of a Swarm task and its Container and Node.
type TaskHealthAggregate struct {
	Task      swarm.Task          `json:","`
	Container types.ContainerJSON `json:","`
	Node      swarm.Node          `json:","`
}

type taskHealthMeta struct {
	TaskID           string                  `json:",omitempty"`
	TaskName         string                  `json:",omitempty"`
	TaskCreatedAt    time.Time               `json:",omitempty"`
	TaskDesiredState swarm.TaskState         `json:",omitempty"`
	TaskState        swarm.TaskState         `json:",omitempty"`
	TaskTimestamp    time.Time               `json:",omitempty"`
	TaskErr          string                  `json:",omitempty"`
	ContainerID      string                  `json:",omitempty"`
	ContainerName    string                  `json:",omitempty"`
	ContainerLabels  map[string]string       `json:",omitempty"`
	NodeID           string                  `json:",omitempty"`
	NodeHostname     string                  `json:",omitempty"`
	HealthCmd        *container.HealthConfig `json:",omitempty"`
	Status           string                  `json:",omitempty"`
	FailingStreak    int                     `json:","`
}

// TaskHealthSummary contains the fields for summarizing the health of a Swarm task.
type TaskHealthSummary struct {
	taskHealthMeta
	Entries []*types.HealthcheckResult `json:",omitempty"`
}

type TaskHealthSummarySlice []TaskHealthSummary

func (t TaskHealthSummarySlice) Len() int {
	return len(t)
}
func (t TaskHealthSummarySlice) Swap(i, j int) {
	t[i], t[j] = t[j], t[i]
}

func (t TaskHealthSummarySlice) Less(i, j int) bool {
	switch {
	case t[i].TaskID == t[j].TaskID:
		return t[i].TaskTimestamp.Before(t[j].TaskTimestamp)
	default:
		return t[i].TaskCreatedAt.Before(t[j].TaskCreatedAt)
	}
}

type TaskHealthResultSlice []TaskHealthResult

// TaskHealthResult contains the fields for a single healthcheck result along with the health summary of a Swarm task.
type TaskHealthResult struct {
	taskHealthMeta
	Result *types.HealthcheckResult `json:","`
}

func (t TaskHealthResultSlice) Len() int {
	return len(t)
}
func (t TaskHealthResultSlice) Swap(i, j int) {
	t[i], t[j] = t[j], t[i]
}

func (t TaskHealthResultSlice) Less(i, j int) bool {
	switch {
	case t[i].TaskID == t[j].TaskID:
		if t[i].Result == nil || t[j].Result == nil {
			return t[i].TaskTimestamp.Before(t[j].TaskTimestamp)
		}
		return t[i].Result.Start.Before(t[j].Result.Start)
	default:
		return t[i].TaskCreatedAt.Before(t[j].TaskCreatedAt)
	}
}
