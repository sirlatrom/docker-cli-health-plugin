package main

import (
	"github.com/docker/cli/cli"
	"github.com/docker/cli/cli-plugins/plugin"
	"github.com/docker/cli/cli/command"
	"github.com/spf13/cobra"
)

// NewRootCmd creates the root command of the plugin.
func NewRootCmd(name string, isPlugin bool, dockerCli command.Cli) *cobra.Command {
	cmd := &cobra.Command{
		Use:   name,
		Short: "Allows inspecting the health of a container inside a task through UCP",
		Args:  cli.NoArgs,
		RunE:  command.ShowHelp(dockerCli.Err()),
		Annotations: map[string]string{
			"version": "1.30",
			"swarm":   "",
		},
	}
	if isPlugin {
		cmd.PersistentPreRunE = func(cmd *cobra.Command, args []string) error {
			return plugin.PersistentPreRunE(cmd, args)
		}
	}

	cmd.AddCommand(
		healthInspectCmd(dockerCli),
		healthLogsCmd(dockerCli),
	)

	return cmd
}
