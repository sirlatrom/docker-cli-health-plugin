package main

import (
	"github.com/docker/cli/cli"
	"github.com/docker/cli/cli/command"
	"github.com/docker/cli/cli/command/formatter"
	"github.com/docker/cli/opts"
	"github.com/spf13/cobra"
)

// HealthLogsOptions contains the options for the `health logs` command
type HealthLogsOptions struct {
	TaskIDs   []string
	Format    string
	Raw       bool
	Filter    opts.FilterOpt
	NoResolve bool
	NoTrunc   bool
}

func healthLogsCmd(dockerCli command.Cli) *cobra.Command {
	options := HealthLogsOptions{Filter: opts.NewFilterOpt()}

	cmd := &cobra.Command{
		Use:     "logs [OPTIONS] SERVICE|TASK [SERVICE|TASK...]",
		Aliases: []string{"i"},
		Short:   "Display health logs of a task",
		Args:    cli.RequiresMinArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			options.TaskIDs = args
			return RunHealthLogs(dockerCli, options)
		},
	}
	flags := cmd.Flags()
	flags.StringVar(&options.Format, "format", "", "Format the output using the given Go template")
	flags.BoolVar(&options.Raw, "raw", false, "Do not neatly format logs")
	flags.BoolVar(&options.NoResolve, "no-resolve", false, "Do not map IDs to Names")
	flags.BoolVar(&options.NoTrunc, "no-trunc", false, "Don't truncate output")
	flags.VarP(&options.Filter, "filter", "f", "Filter output based on conditions provided")

	return cmd
}

// RunHealthLogs prints task health logs.
func RunHealthLogs(dockerCli command.Cli, options HealthLogsOptions) error {
	client := dockerCli.Client()

	summaries, _, err := buildTaskHealthSummaries(client, options.TaskIDs, options.NoResolve, options.Filter.Value())
	if err != nil {
		return err
	}
	results := TaskHealthResultSlice{}
	for _, summary := range summaries {
		if len(summary.Entries) == 0 {
			results = append(results, TaskHealthResult{
				taskHealthMeta: summary.taskHealthMeta,
			})
			continue
		}
		for _, entry := range summary.Entries {
			results = append(results, TaskHealthResult{
				taskHealthMeta: summary.taskHealthMeta,
				Result:         entry,
			})
		}
	}

	format := options.Format
	if len(format) == 0 {
		if len(dockerCli.ConfigFile().ConfigFormat) > 0 && !options.Raw {
			format = dockerCli.ConfigFile().ConfigFormat
		} else {
			format = formatter.TableFormatKey
		}
	}

	healthLogsCtx := formatter.Context{
		Output: dockerCli.Out(),
		Format: NewFormat(format, options.Raw),
		Trunc:  !options.NoTrunc,
	}
	return FormatWrite(healthLogsCtx, results, options.Filter.Value())
}
